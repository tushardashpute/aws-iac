# Creating random password
resource "random_password" "password" {
  length           = 16
  special          = true
  override_special = "!#$%&*()-_=+[]{}<>:?"
}


resource "null_resource" "create-endpoint" {
  provisioner "local-exec" {
    command = "aws iam update-login-profile --user-name Bob --password ${random_password.password.result}"
  }
}
